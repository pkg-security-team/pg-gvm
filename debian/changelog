pg-gvm (22.6.5-2) unstable; urgency=medium

  * Update for PostgreSQL 17 (Closes: #1082120)
  * Bump Standards-Version to 4.7.0 (no change)

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 19 Sep 2024 10:23:24 +0200

pg-gvm (22.6.5-1) unstable; urgency=medium

  * New upstream version 22.6.5
  * Replace obsolete package pkg-config with pkgconf
  * Make the build reproducible (Closes: #1068173), thanks to Chris Lamb

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 23 Apr 2024 16:16:48 +0200

pg-gvm (22.6.2-1) unstable; urgency=medium

  [ Christoph Berg ]
  * Convert to PostgreSQL extension packaging standards (Closes: #1052177):
    + Build for all supported PostgreSQL versions using pg_buildext.
    + Rename binary package to postgresql-PGVERSION-pg-gvm.
    + Conflicts/Replaces: pg-gvm.
  * debian/tests: Run regression tests using pgtap.

  [ Sophie Brun ]
  * New upstream version 22.6.2
  * Update minimal required version of libgvm-dev
  * Update debian/copyright
  * Update Breaks and Replaces versions for compatibility with Kali packages

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 19 Sep 2023 10:36:16 +0200

pg-gvm (22.4.0-2) unstable; urgency=medium

  * Source -only upload

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 23 Jan 2023 10:16:43 +0100

pg-gvm (22.4.0-1) unstable; urgency=medium

  * Initial release (Closes: #1026796)

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 28 Dec 2022 09:54:18 +0100
